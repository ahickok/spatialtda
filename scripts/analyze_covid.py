# -*- coding: utf-8 -*-
"""
Analyze geospatial covid data using levelset method.
"""
import geopandas as gpd
import xlrd
import levelset as ls
import matplotlib.pyplot as plt
import persistence_utils
import gudhi as gd

def compute_CA_persistence(date, threshold):
    mdy = date.split("/")
    write_CA_data(date)    # writes (or overwrites) 'cases' data to CA county shpfile
    tiff_filename = f'data/CA_counties/CA_counties_cases_{threshold}_date_{mdy[0]}_{mdy[1]}_{mdy[2]}.tif'
    ls.rasterize_by_threshold('data/CA_counties/CA_counties.shp', 'cases', threshold, 250, tiff_filename)
    compute_ls_persistence_from_tiff(tiff_filename)
    
def compute_LA_persistence(threshold):
    la_shp = 'data/LA_nbhds/COVID19_by_Neighborhood.shp'
    tiff_filename = ls.rasterize_by_threshold(la_shp, 'cases', threshold, 250)
    compute_ls_persistence_from_tiff(tiff_filename)
    
def compute_ls_persistence_from_tiff(tiff):
    img_array = plt.imread(tiff)
    simplex_list, entry_times = ls.build_levelset_complex(img_array, None)
    _, st = persistence_utils.compute_levelset_persistence(simplex_list, entry_times)
    ph = st.persistence_intervals_in_dimension(1)
    gd.plot_persistence_diagram(persistence = ph)
    print(ph)
    
def read_CA_data(county_names, date):
    # county_names is a list of the county names we're interested in.
    # date is a string in the format 'month/day/year' (e.g. "1/22/20", which is 
    # the earliest date in the file, or "7/8/20", which is the last date.
    # Returns: Dictionary. Keys are county names. Values are case counts on date.
    file = xlrd.open_workbook('data/covid_usafacts.xlsx')
    sheet = file.sheet_by_index(0)
    date_col = 0
    while not sheet.cell_value(0, date_col) == date:
        date_col += 1
    CA_rows = range(192, 252)
    county_values = {}
    for i in CA_rows:
        county = str(sheet.cell_value(i, 1)).split()[:-1]
        s = " "
        county = s.join(county)
        if county in county_names:
            county_values.update({county : sheet.cell_value(i, date_col)})
    return county_values
  
def write_CA_data(date):
    # Writes 'cases' attribute to shp file
    ca_shpfile = 'data/CA_counties/CA_counties.shp'
    namecol = 'NAME'
    ca_gdf = gpd.read_file(ca_shpfile)
    county_names = list(ca_gdf[namecol])
    ca_data = read_CA_data(county_names, date)
    num_regions = len(ca_gdf.geometry)
    ca_gdf['cases'] = [ca_data[ca_gdf[namecol][i]] for i in range(num_regions)]
    ca_gdf.to_file(ca_shpfile)