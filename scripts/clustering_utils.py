from pyclustering.cluster.kmedoids import kmedoids
import re
import pandas as pd
import scripts.config as config
import numpy as np
from scipy.cluster.hierarchy import linkage, fcluster
from scipy.spatial.distance import squareform

def cluster_diags(base_dir, application, clustering_type, N):
    dist_mat = np.load(f'{base_dir}results/dist_mat/{application}distance.npy')
    dist_mat[dist_mat == np.inf] = np.max(dist_mat[dist_mat != np.inf])+1
    clusters = [[] for i in range(N)]
    if clustering_type == config.ClusteringType.MEDOID:
        initial_medoids = [1, 2, 5]
        kmedoids_instance = kmedoids(dist_mat, initial_medoids, data_type='distance_matrix')
        kmedoids_instance.process()
        clusters = kmedoids_instance.get_clusters()
        linkage_mat = [[]]
    if clustering_type == config.ClusteringType.HEIR:
        dist_mat = squareform(dist_mat)
        linkage_mat = linkage(dist_mat, method='average')
        if N < dist_mat.shape[0]:
            flat_cluster = fcluster(linkage_mat, t=linkage_mat[-N,2], criterion='distance')
        else:
            flat_cluster = fcluster(linkage_mat, t=0, criterion='distance')
        for i in range(len(flat_cluster)):
            clusters[flat_cluster[i]-1].append(i)
    return clusters, linkage_mat


def cluster_list_to_df(clusters, file_key, application):
    if application == 'city/inter/':
        neighborhood_list = []
        index_list = []
    elif application == 'city/intra/':
        city_list = []
    cluster_number = []
    for i in range(len(clusters)):
        for index in clusters[i]:
            if application == 'city/inter/':
                if '-' in file_key[index]:
                    neighborhood_list.append(re.sub('-[0-9]*', '', file_key[index]))
                    index_list.append(re.sub('\D*-', '', file_key[index]))
                else:
                    neighborhood_list.append(file_key[index])
                    index_list.append('0')
            elif application == 'city/intra/':
                city_list.append(file_key[index])
            cluster_number.append(i)
    if application == 'city/inter/':
        cluster_info_dict = {'neighborhood': neighborhood_list, 'index': index_list, 'cluster_number': cluster_number}
        cluster_df = pd.DataFrame(cluster_info_dict)
        cluster_df['index'] = pd.to_numeric(cluster_df['index'])
    if application == 'city/intra/':
        cluster_info_dict = {'city_ascii': city_list, 'cluster_number': cluster_number}
        cluster_df = pd.DataFrame(cluster_info_dict)
    return cluster_df


def find_cluster_percentages_by_p0(clusters, file_key):
    p0 = np.zeros(len(file_key))
    fcluster = np.zeros(len(file_key))
    per_dict = {}
    for cluster_num in range(len(clusters)):
        for i in clusters[cluster_num]:
            filename = file_key[i]
            params = filename.split('-')
            p0[i] = float(params[7])
            fcluster[i] = cluster_num
    for p0_val in np.linspace(0.05, 0.3, 6):
        p0_val = float('%.3f'%p0_val)
        per_dict[p0_val] = np.sum(fcluster[p0 == p0_val])/50
    return per_dict