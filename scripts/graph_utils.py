import scripts.config as config
from networkx.generators import random_geometric_graph, grid_graph, watts_strogatz_graph
import matplotlib.pyplot as plt
import networkx as nx
from networkx.drawing.layout import spring_layout
from numpy.random import randint
import numpy as np


def get_graph(base_dir, application_type, instance, **kwargs):
    if application_type == config.ApplicationType.RGG:
        application = 'rgg/'
        n = kwargs.get('num_verts', 100)
        radius = kwargs.get('radius', .1)
        G = random_geometric_graph(n, radius)
        pos=nx.get_node_attributes(G,'pos')
    elif application_type == config.ApplicationType.LATTICE:
        application = 'lattice/'
        n_edge = kwargs.get('num_verts_edges', 10)
        n = n_edge*n_edge
        G = grid_graph(dim=[n_edge, n_edge])
        G = nx.convert_node_labels_to_integers(G)
        pos = nx.spring_layout(G)
    elif application_type == config.ApplicationType.WS:
        application = 'ws/'
        n = kwargs.get('num_verts', 100)
        k = kwargs.get('k', 5)
        p = kwargs.get('p', .1)
        G = watts_strogatz_graph(n, k, p)
        G = nx.convert_node_labels_to_integers(G)
        pos = nx.circular_layout(G)
    phi = kwargs.get('phi', .18)
    seed_fraction = kwargs.get('seed_fraction',.1)
    infection_time = watts_threshold(G, phi, seed_fraction)
    nx.set_node_attributes(G, infection_time, 'inftime')
    inf_time_list = [int(i) for i in list(infection_time.values())]
    fig, ax = plt.subplots()
    nx.draw(G, ax=ax, pos=pos, node_size=50, widths=.1, linewidths=.1, node_color=inf_time_list)
    plt.savefig(f'{base_dir}figures/raw/{application}N-{n}-phi-{phi}-p0-{seed_fraction}-{instance}.png')
    plt.close()
    nx.write_gml(G, f'{base_dir}data/raw/{application}N-{n}-phi-{phi}-p0-{seed_fraction}-{instance}.gml')
    return G


def watts_threshold(G, phi, seed_fraction, MAX_STEPS = 10000):
    num_nodes = len(G.nodes())
    infected_time = np.array([MAX_STEPS]*num_nodes)
    num_infected = int(seed_fraction * num_nodes)
    is_infected = np.zeros(num_nodes)

    init_infected = np.array(randint(0, num_nodes, num_infected))
    infected_time[init_infected] = 0
    is_infected[init_infected] = 1

    adj_mat = np.array(nx.to_numpy_matrix(G))
    curr_step = 0
    while curr_step < MAX_STEPS and infected_time[infected_time == curr_step].size > 0:
        curr_step += 1
        numerator = adj_mat @ is_infected
        denom = np.sum(adj_mat, axis=0)
        newly_infected = np.logical_and(numerator/denom >= phi, infected_time == MAX_STEPS)
        infected_time[newly_infected] = curr_step
        is_infected[newly_infected] = 1
    infected_time[infected_time == MAX_STEPS] = curr_step + 5
    return {i: str(infected_time[i]) for i in range(num_nodes)}


