from enum import Enum, auto


class SimplexType(Enum):
    LS = auto()
    ADJV = auto()
    ADJE = auto()


class ApplicationType(Enum):
    INTERCITY = auto()
    INTRACITY = auto()
    SPIDERS = auto()
    SNOWFLAKES = auto()
    RGG = auto()
    LATTICE = auto()
    WS = auto()
    FIGS = auto()


class ClusteringType(Enum):
    SPECTRAL = auto()
    MEDOID = auto()
    HEIR = auto()


base_dir = '../'
env_dir = '/Users/michellefeng/anaconda3/envs/spatialtda/'
application_type = ApplicationType.FIGS
simplex_type = SimplexType.LS


folder_structure = {
    'figures': {
        'density': [],
        'diagrams': [],
        'levelset': ['2d', '3d'],
        'vis': [],
        'clusters': [],
        'raw': []
    },
    'data': ['sc', 'sample', 'raw'],
    'results': ['diagram', 'keys', 'dist_mat']
}
run_setup = False
rgg_setup = {
    'num_instances': 100,
    'num_verts': 100,
    'radius': 0.1,
    'phi': 0.18,
    'seed_fraction': 0.05,
}

lattice_setup = {
    'num_instances': 100,
    'num_verts_edge': 10,
    'phi': 0.18,
    'seed_fraction': 0.20,
}

ws_setup = {
    'num_instances': 100,
    'n': 100,
    'k': 5,
    'p': .1,
    'phi': 0.18,
    'seed_fraction': 0.1,
}


ls_workflow = {
    'save_only_new_images': True,
    'compute_levelset': True,
    'gen_sc_vis': True,
    'compute_persistence': False,
    'plot_diagram': False,
    'plot_density': False,
    'compute_bottleneck': False,
    'compute_clusters': False,
    'plot_clusters_on_map': False,
    'clustering_type': ClusteringType.HEIR,
    'num_clusters': 12,
    'plot_dendrogram': False
}


adjv_workflow = {
    'compute_persistence': False,
    'use_computed_diag': True,
    'plot_diagram': False,
    'plot_density': False,
    'compute_bottleneck': False,
    'compute_clusters': False,
    'clustering_type': ClusteringType.HEIR,
    'num_clusters': 2,
    'plot_dendrogram': False
}
