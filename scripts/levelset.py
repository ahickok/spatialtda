import gdal
from osgeo import ogr, osr
import numpy as np
import matplotlib.pyplot as plt
import invr
import time
import logging
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from PIL import Image

def rasterize_by_threshold(shp, attribute, threshold, max_pixels, out_tiff = None):
    '''
    Parameters
    ----------
    shp : String
        .shp filename
    attribute : String
        Name of an attribute in the shp file that has numeric values.
    threshold : Number.
        We rasterize the shp file s.t. for features whose attribute is below 
        threshold, the pixels are black.
    max_pixels : int
        Max number of pixels for height/width of rasterized shp file.
    out_tiff : String, optional
        tiff filename to save to. Saves to shp.split(".")[0] + f"_{attribute}_{threshold}.tif" if None.
        
    Returns
    -------
    out_tiff :  String.
        Name of tiff filename where we save the rasterized shp file.
    '''
    # Delete the 'isUnder' field if it exists. Fields don't get overwritten, so if we don't do this it will keep making duplicate fields.
    source_ds = gdal.OpenEx(shp, gdal.OF_VECTOR | gdal.OF_UPDATE)
    source_ds.ExecuteSQL("ALTER TABLE " + shp.split(".")[0].split("/")[-1] + " DROP COLUMN isUnder")
    
    source_ds = ogr.Open(shp, 1)
    source_layer = source_ds.GetLayer()
    
    fd = ogr.FieldDefn('isUnder', ogr.OFTInteger)
    source_layer.CreateField(fd)
    for feat in source_layer:
        below_thresh = (feat.GetField(attribute) < threshold)
        # Note- field names need to be <= 10 chars, which is why we can't call it 'below_thresh'
        feat.SetField('isUnder', (1 - int(below_thresh)) * 255)
        source_layer.SetFeature(feat)
    
    x_min, x_max, y_min, y_max = source_layer.GetExtent()
    max_cols = max_rows = max_pixels
    max_pixel_width = (x_max - x_min) / max_cols
    max_pixel_height = (y_max - y_min) / max_rows
    pixel_width = pixel_height = max(max_pixel_width, max_pixel_height)
    cols = int((x_max - x_min) / pixel_height)
    rows = int((y_max - y_min) / pixel_width)
    
    if out_tiff is None: out_tiff = shp.split(".")[0] + f"_{attribute}_{threshold}.tif"
    target_ds = gdal.GetDriverByName('Gtiff').Create(out_tiff, cols, rows, 1, gdal.GDT_Byte)
    target_ds.SetGeoTransform((x_min, pixel_width, 0, y_max, 0, -pixel_height))
    band = target_ds.GetRasterBand(1)
    no_data_value = 255
    band.SetNoDataValue(no_data_value)
    band.FlushCache()

    gdal.RasterizeLayer(target_ds, [1], source_layer, options=["ATTRIBUTE=isUnder"])
    target_ds_srs = osr.SpatialReference()
    target_ds_srs.ImportFromEPSG(4326)
    target_ds.SetProjection(target_ds_srs.ExportToWkt())
    
    source_ds = gdal.OpenEx(shp, gdal.OF_VECTOR | gdal.OF_UPDATE)
    source_ds.ExecuteSQL("ALTER TABLE " + shp.split(".")[0].split("/")[-1] + " DROP COLUMN isUnder")
    return out_tiff

def rasterize_shp(county, candidate, max_pixels):
    shp = '../data/shapefiles/' + county + '.shp'

    source_ds = gdal.OpenEx(shp, gdal.OF_VECTOR | gdal.OF_UPDATE)
    source_ds.ExecuteSQL("ALTER TABLE "+county+" DROP COLUMN isHill")

    source_ds = None

    source_ds = ogr.Open(shp, 1)
    source_layer = source_ds.GetLayer()

    fd = ogr.FieldDefn('isHill', ogr.OFTInteger)
    source_layer.CreateField(fd)

    for feat in source_layer:
        if candidate == 'hillary':
            is_hill = (feat.GetField('Hill%') > 0)
        elif candidate == 'trump':
            is_hill = (feat.GetField('Hill%') < 0)
        feat.SetField('isHill', (1 - int(is_hill)) * 255)
        source_layer.SetFeature(feat)

    x_min, x_max, y_min, y_max = source_layer.GetExtent()
    max_cols = max_rows = max_pixels
    max_pixel_width = (x_max - x_min) / max_cols
    max_pixel_height = (y_max - y_min) / max_rows
    pixel_width = pixel_height = max(max_pixel_width, max_pixel_height)
    cols = int((x_max - x_min) / pixel_height)
    rows = int((y_max - y_min) / pixel_width)

    out_tiff = '../data/tif/' + candidate + '/' + county + '.tif'

    target_ds = gdal.GetDriverByName('Gtiff').Create(out_tiff, cols, rows, 1, gdal.GDT_Byte)
    target_ds.SetGeoTransform((x_min, pixel_width, 0, y_max, 0, -pixel_height))
    band = target_ds.GetRasterBand(1)
    no_data_value = 255
    band.SetNoDataValue(no_data_value)
    band.FlushCache()

    gdal.RasterizeLayer(target_ds, [1], source_layer, options=["ATTRIBUTE=isHill"])
    target_ds_srs = osr.SpatialReference()
    target_ds_srs.ImportFromEPSG(4326)
    target_ds.SetProjection(target_ds_srs.ExportToWkt())

    source_ds = None

    source_ds = gdal.OpenEx(shp, gdal.OF_VECTOR | gdal.OF_UPDATE)
    source_ds.ExecuteSQL("ALTER TABLE "+county+" DROP COLUMN isHill")

    source_ds = None


def godunov(phi, a):
    phi_p_x = np.zeros(phi.shape)
    phi_n_x = np.zeros(phi.shape)
    phi_p_y = np.zeros(phi.shape)
    phi_n_y = np.zeros(phi.shape)

    phi_p_x[:, 0:-1] = phi[:, 1:] - phi[:, 0:-1]
    phi_n_x[:, 1:] = phi[:, 1:] - phi[:, 0:-1]
    phi_p_y[0:-1, :] = phi[1:, :] - phi[0:-1, :]
    phi_n_y[1:, :] = phi[1:, :] - phi[0:-1, :]

    phi_x_neg = np.maximum(np.power(np.maximum(phi_n_x, 0), 2), np.power(np.minimum(phi_p_x, 0), 2))
    phi_y_neg = np.maximum(np.power(np.maximum(phi_n_y, 0), 2), np.power(np.minimum(phi_p_y, 0), 2))

    phi_x_pos = np.maximum(np.power(np.minimum(phi_n_x, 0), 2), np.power(np.maximum(phi_p_x, 0), 2))
    phi_y_pos = np.maximum(np.power(np.minimum(phi_n_y, 0), 2), np.power(np.maximum(phi_p_y, 0), 2))

    phi_x = -np.minimum(np.sign(a), 0) * phi_x_neg + np.maximum(np.sign(a), 0) * phi_x_pos
    phi_y = -np.minimum(np.sign(a), 0) * phi_y_neg + np.maximum(np.sign(a), 0) * phi_y_pos
    return np.sqrt(phi_x + phi_y)


def sign(phi):
    denom = np.sqrt(np.power(phi, 2) + godunov(phi, np.ones(phi.shape)))
    return np.divide(phi, denom)


def normalize_distance(image, time):
    length, width = image.shape
    c = np.sqrt(length ^ 2 + width ^ 2)
    distances = image.copy()
    distances = distances.astype(np.float)
    distances = ((distances / 255) * 2 - 1) * c
    sign_phi = sign(distances)
    dt = 1 / (4 * np.max(np.abs(sign_phi)))

    for i in np.arange(time / dt):
        dphi = dt * sign_phi * (1 - godunov(distances, -sign_phi))
        distances = distances + dphi

    return distances


def advance_level_set(image, time):
    a = np.ones(image.shape)
    dt = .125

    for i in np.arange(time / dt):
        dphi = -dt * a * godunov(image, a)
        image = image + dphi

    return image


def write_img_idx_map(image, existing_verts, entry_times, T, skip=5):
    curr_verts = np.flatnonzero(image==0)
    r,c = image.shape
    curr_verts = [vert for vert in curr_verts if ((vert%c)%skip==0 and (int(vert/c))%skip==0)]
    new_verts = list(set(curr_verts).difference(set(existing_verts.keys())))
    n = len(existing_verts)
    for vert in new_verts:
        existing_verts[vert] = n
        n += 1
        entry_times.append(T)
    return existing_verts, entry_times


def gen_img_adjacencies(existing_verts, h, w, skip=5):
    existing_verts_list = list(existing_verts.keys())
    adjacencies = [[]]*len(existing_verts)
    for vert in existing_verts_list:
        poss_adj = [vert-skip*w-skip, vert-skip*w, vert-skip, vert+skip, vert+skip*w, vert+skip*w+skip]
        poss_adj = [i for i in poss_adj if i>=0]
        poss_adj = list(set(poss_adj).intersection(set(existing_verts_list)))
        adjacencies[existing_verts[vert]] = [existing_verts[neighbor] for neighbor in poss_adj]
    return adjacencies

def build_levelset_complex(img_array, img_name, save_2d_plots = False, save_3d_plots = False, write_key = False, save_sc = False, save_2d_dir = '../figures/levelset/2d/', save_3d_dir = '../figures/levelset/3d/', key_dir='../results/keys/', img_extension='.png', sc_dir = '../data/sc/', skip_int=5):
    dT = .5
    max_T = 50
    n = 20
    T = 0
    i = 0
    existing_verts = {}
    entry_times = []
    assert len(img_array.shape) == 2, "Image should be grayscale"
    existing_verts, entry_times = write_img_idx_map(img_array, existing_verts, entry_times, T)

    gamma = 7
    img_array = normalize_distance(img_array, gamma)
    while T < max_T and np.count_nonzero(img_array >= 0) > 0:
        T = T + dT
        img_array = advance_level_set(img_array, dT)
        phi_array_img = (np.sign(img_array) + 1) / 2 * 255


        if save_2d_plots:
            plt.imsave(save_2d_dir + img_name + '-' + str(T) + img_extension, (-img_array>0))
            plt.close()

        if save_3d_plots:
            xy = np.indices(img_array.shape)
            xdata = xy[0]
            ydata = xy[1]

            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(xdata, ydata, -img_array, linewidth=.5, alpha=0.1)
            ax.plot_surface(xdata, ydata, np.zeros(img_array.shape), alpha=0.1)
            ax.plot_wireframe(xdata, ydata, -img_array, linewidth=0.5, rstride=10, cstride=10, alpha=0.5)
            ax.contour(xdata, ydata, -img_array, levels=[0], offset=0, cmap=cm.Spectral)
            ax.view_init(30, 0)
            ax.set(zlim=(-16,16))
            ax.axis('off')
            plt.savefig(save_3d_dir + img_name + '-' + str(T) + img_extension, bbox_inches='tight')
            plt.close()

        if i % 5 == 0:
            img_array = normalize_distance(phi_array_img, gamma)
        existing_verts, entry_times = write_img_idx_map(phi_array_img, existing_verts, entry_times, T, skip=skip_int)
        i = i+1
    phi_array_img = np.zeros(img_array.shape)
    phi_array_img = phi_array_img.astype(int)
    existing_verts, entry_times = write_img_idx_map(phi_array_img, existing_verts, entry_times, T+dT, skip=skip_int)

    if write_key:
        key_write_filename = key_dir + img_name
        with open(key_write_filename, 'w') as key_file:
            for k, v in existing_verts.items():
                key_file.write(str(k)+','+str(v)+'\n')
        key_file.close()

    h, w = img_array.shape
    adjacencies = gen_img_adjacencies(existing_verts, h, w, skip=skip_int)

    maxDimension = 3
    V = []
    V = invr.incremental_vr(V, adjacencies, maxDimension)
    entry_times = [int(time*2) for time in entry_times]

    if save_sc:
        phatFormatV = invr.replace_face(V)
        entryTimesSub = [entry_times[max(simplex)-1] for simplex in V]

        np.savetxt(sc_dir + img_name + '_entry_times.csv',entryTimesSub,
                   delimiter=' ',fmt='%i')

        F = open(sc_dir + img_name + ".dat", "w")

        for face in phatFormatV:
            F.write(str(len(face) - 1))
            if len(face) > 1:
                for simplex in face:
                    F.write(" " + str(simplex))
            F.write("\n")
        F.close()
    return V, entry_times


def main():
    logging.basicConfig(filename='../logs/ls.log', filemode='w+', level=logging.WARNING)
    timing_csv = '../runtimes/ls_times.csv'
    with open(timing_csv,'w+') as timing_file:
        with open('../full-list') as county_file:
            # for county in county_file:
            for county in ['081-san-mateo']:
                county = county.split('\n')[0]
                for candidate in ['trump','hillary']:
                    start_time = time.time()
                    build_levelset_complex(county, candidate, True, True)
                    timing_file.write(county + ',ls,'+candidate+','+str(time.time()-start_time)+'\n')


if __name__ == '__main__':
    main()
