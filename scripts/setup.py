import osmnx as ox
import os
import scripts.config as config
import scripts.sample_city as sample_city
import pyproj
from scripts.graph_utils import get_graph


def save_city_img(name, coord, size=240, dpi=40, extension='png'):
    try:
        ox.plot_figure_ground(point=coord, filename=name, network_type='drive', dpi=dpi)
    except Exception:
        pass


def create_folder_structure(folder_dict, application, curr_string):
    for k, v in folder_dict.items():
        if isinstance(v, dict):
            create_folder_structure(v, application, f'{curr_string}{k}/')
        elif isinstance(v, list):
            if not v:
                dir_string = f'{curr_string}{k}/{application}/'
                if not os.path.exists(dir_string):
                    os.makedirs(dir_string)
            else:
                for item in v:
                    dir_string = f'{curr_string}{k}/{item}/{application}/'
                    if not os.path.exists(dir_string):
                        os.makedirs(dir_string)


def city_setup(base_dir, env_dir, application_type, folder_dict, **kwargs):
    pyproj.datadir.set_data_dir(f'{env_dir}share/proj')
    img_dir = f'{base_dir}figures/raw/city/'

    if application_type == config.ApplicationType.INTERCITY:
        place_dict = sample_city.intercity_dict(config.base_dir, 'shanghai', 10)
        img_dir = f'{img_dir}inter/'
        create_folder_structure(folder_dict, 'city/inter/', base_dir)
    elif application_type == config.ApplicationType.INTRACITY:
        place_dict = sample_city.intracity_dict(base_dir)
        img_dir = f'{img_dir}intra/'
        create_folder_structure(folder_dict, 'city/intra/', base_dir)
    else:
        raise ValueError('Invalid City Comparison Type')

    ox.utils.config(imgs_folder=img_dir)

    for place_name, coordinate in place_dict.items():
        if kwargs.get('save_only_new_images') and os.path.exists(f'{img_dir}{place_name}.png'):
            pass
        else:
            save_city_img(place_name, coordinate)


def spider_setup(base_dir, folder_dict):
    create_folder_structure(folder_dict, 'spiders', base_dir)


def snowflake_setup(base_dir, folder_dict):
    create_folder_structure(folder_dict, 'snowflakes', base_dir)


def rgg_setup(base_dir, folder_dict, **kwargs):
    create_folder_structure(folder_dict, 'rgg', base_dir)

    for i in range(kwargs.get('num_instances', 50)):
        get_graph(base_dir, config.ApplicationType.RGG, i, **kwargs)


def lattice_setup(base_dir, folder_dict, **kwargs):
    create_folder_structure(folder_dict, 'lattice', base_dir)

    for i in range(kwargs.get('num_instances', 50)):
        get_graph(base_dir, config.ApplicationType.LATTICE, i, **kwargs)


def ws_setup(base_dir, folder_dict, **kwargs):
    create_folder_structure(folder_dict, 'ws', base_dir)

    for i in range(kwargs.get('num_instances', 50)):
        get_graph(base_dir, config.ApplicationType.WS, i, **kwargs)


